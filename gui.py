from tkinter import *
from tkinter import ttk

root = Tk()

frame = ttk.Frame(root)

# Set root window size
width, height = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry('%dx%d+0+0' % (width,height))

# Run dat app
root.mainloop()